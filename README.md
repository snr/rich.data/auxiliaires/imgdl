# IMGDL - module d'export de ressources iconographiques de la base de données

Ce petit CLI bien pratique permet de réaliser un 
export CSV+XLSX des entrées iconographiques voulues, ainsi que de télécharger
toutes les images liées. Les entrées de la table iconographique sont ciblées à
partir d'un fichier `txt` contenant des identifiants uniques (correspondant à 
`iconography.id_uuid` dans la base de données), avec un identifiant par ligne.
Tout le produit est zippé et sauvegardé dans `out/`.

---

## Utilisation

### Prérequis

Pour utiliser le CLI, il faut un accès à la base de données (locale ou non) et 
un accès au serveur contenant toutes les images de la table iconographique (serveur
local ou non). 

Les identifiants de la base de données doivent être stockés dans
le fichier `./confidentials/postgresql_credentials.json` avec la structure suivante:

```python
{
  "username": "...",
  "password": "...",
  "uri": "...",
  "db": "..."
}
```

### Utilisation

#### En bref

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt

python imgdl.py -u "local|remote" -f "filename"
```

#### Options possibles

- `-u --urlcontext` (**string**): une de deux valeurs (`local|remote`), qui permet de sélectionner
  l'URL à laquelle les images seront récupérées. 
  - Si c'est un serveur local 
    [comme celui-ci](https://gitlab.inha.fr/snr/rich.data/application/-/tree/main/staticserver?ref_type=heads), 
    s'assurer qu'il est lancé; si c'est un serveur distant, s'assurer qu'on y a accès.
- `-f --filename` (**string**): le chemin vers le fichier contenant les identifiants. 
   - Pour rappel, ce fichier doit contenir identifiant par ligne, rien de plus
     (voir l'exemple dans `in/`).
- `-q --fullquality` (**bool**): gérer la qualité des images téléchargées.
   - si `-q` est spécifié, seules les images
     dans la meilleure qualité possible seront téléchargées. 
   - sinon, 3 versions de chaque image sera téléchargée (HD, compressée, thumbnail)

---

## License

GNU GPL 3.0 :)
