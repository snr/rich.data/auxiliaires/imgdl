-- generate a CSV representation of the iconography table
-- this is a template where the placeholder in the WHERE clause needs to be
-- replaced by a series of comma-separated, single-quoted
-- iconography.id_uuid, in python

SELECT iconography.id_uuid
          AS "id",
       -- array_agg(DISTINCT x) turns rows into an array and deduplicates it
       array_to_string( array_agg(DISTINCT author.entry_name), ' | ')
          AS "auteur.ice.s",
       array_to_string( array_agg(DISTINCT publisher.entry_name), ' | ')
          AS "édition",
       -- returns the 1st title
       (array_agg(title.entry_name))[1]
          AS "titre",
       -- returns the date as a string with format:
       -- AAAA-AAA or AAAA
       -- process: turn int4range into int[], deduplicate it and stringify it
       -- for the array deduplication: https://collectingwisdom.com/postgresql-remove-duplicate-values-from-array/
       array_to_string(
         (SELECT array_agg(DISTINCT val)
          FROM ( SELECT UNNEST(ARRAY[ lower(iconography.date)
                                    , upper(iconography.date)-1
                                    ]) AS val
               ) AS s), '-' )
          AS "date",
       array_to_string( iconography.technique, ' | ' )
         AS "technique",
       array_to_string(
         array_agg(DISTINCT institution.entry_name), ' | ' )
         AS "institution(s) de conservation",
       (array_agg(iconography.source_url))[1],
       array_to_string( array_agg(DISTINCT filename.url), ' | ' )
          AS "fichier(s)"

-- we only use left joins to be sure to fetch
-- all iconography rows, even where there's
-- no data in the join tables.
FROM iconography
LEFT JOIN  title
ON title.id_iconography = iconography.id
LEFT JOIN r_institution
ON r_institution.id_iconography = iconography.id
LEFT JOIN institution
ON institution.id = r_institution.id_institution
JOIN filename
ON filename.id_iconography = iconography.id
-- 1st join to actors: authors
LEFT JOIN r_iconography_actor AS ria1
ON ria1.id_iconography = iconography.id
AND ria1.role = 'author'
LEFT JOIN actor AS author
ON author.id = ria1.id_actor
-- 2nd join to actors: publishers
LEFT JOIN r_iconography_actor AS ria2
ON ria2.id_iconography = iconography.id
AND ria2.role = 'publisher'
LEFT JOIN actor AS publisher
ON publisher.id = ria2.id_actor

-- percent-s is a placeholder that will need to be
-- replaced by a series of comma-separated, single-quoted iconography.id_uuids
WHERE iconography.id_uuid IN ( %s )

-- group by to have only one row per iconography entry
GROUP BY iconography.id_uuid,
         iconography.date,
         iconography.technique
;
