"""
take as an input a list of iconography.id_uuid, return
- a CSV/XLSX table containing metadata on these ids
- all images related to these ids
and save the output in a ZIP.
"""

from sqlalchemy.engine import Engine, create_engine
import pandas as pd
import requests
import tqdm

from datetime import datetime
from zipfile import ZipFile
from uuid import uuid4
import zipfile as zf
import typing as t
import argparse
import shutil
import json
import os
import re

# *****************************************

# constants
ROOT          = os.path.abspath(os.path.dirname(__file__))
IN            = os.path.join(ROOT, "in/")
OUT           = os.path.join(ROOT, "out/")
SQL           = os.path.join(ROOT, "sql")
CONFIDENTIALS = os.path.join(ROOT, "confidentials/")

# *****************************************

class ImageDownloader:
    fullquality: bool
    engine     : Engine
    pathout    : os.PathLike | str
    pathtmp    : os.PathLike | str
    idlist     : t.List[str]
    sqlquery   : str
    df         : pd.DataFrame
    url        : str

    def __init__(self, fp:str, urlctx:str, fullquality:bool):
        self.fullquality = fullquality

        # access the filepath and validate its contents
        try:
            with open(fp, mode="r") as fh:
                txt = fh.read()
        except FileNotFoundError:
            raise FileNotFoundError(f": argument -f --filename points to non-existing file `{fp}` !")
        self.idlist = [ t.strip() for t in txt.split("\n") if len(t.strip()) ]
        assert all(re.search("^qr1[a-z0-9]{32}$", t) for t in self.idlist), \
               f"invalid format in input file: expected one UUID per line, each UUID matching the regex 'qr1[a-z0-9]{32}"

        # build the engine
        with open(os.path.join(CONFIDENTIALS, "postgresql_credentials.json"), mode="r") as fh:
            credentials = json.load(fh)
        self.engine = create_engine(
            f"postgresql://{ credentials['username'] }:{ credentials['password'] }@{ credentials['uri'] }/{ credentials['db'] }",
            echo=False
        )

        # read the sql query and remplace the `%s` with the id_uuids
        with open(os.path.join(SQL, "iconography_export_template.sql"), mode="r") as fh:
            self.sqlquery = fh.read()
        idlist_str = ",\n".join(f"'{i}'" for i in self.idlist)
        self.sqlquery = self.sqlquery % idlist_str

        # define the URL
        if urlctx == "local":
            self.url = "http://127.0.0.1:9999/iconography/"
        else:
            self.url = "https://quartier-richelieu-retour.inha.fr/statics/iconography/"
        print(f"* running queries on URL : {self.url}")

        # build the temp directory the output dir and zip file
        self.pathtmp = os.path.join(ROOT, f"tmp_{uuid4()}")
        os.makedirs(self.pathtmp)
        os.makedirs(os.path.join(self.pathtmp, "images"))
        os.makedirs(OUT, exist_ok=True)
        timestamp = datetime.now().strftime(r'%Y%m%d_%H_%M')
        self.pathout = os.path.join(OUT, f"export_{timestamp}")

        return


    def pipeline(self):
        try:
            self.querier().downloader().zipper()
        except Exception as e:
            raise e
        finally:
            # delete all temp files at the end
            shutil.rmtree(self.pathtmp)
        return self


    def querier(self):
        """populate `self.df` by running the sql query"""
        print("* retrieving metadata from the database")
        self.df = pd.read_sql(self.sqlquery, self.engine)
        # remove from the response all the images that are not in full quality
        if self.fullquality:
            self.df["fichier(s)"] = (
                self.df["fichier(s)"]
                .str.split(r"\s*\|\s*", regex=True)                                   # split string to list
                .apply(lambda x: [ f for f in x                                       # remove non full entries
                                   if not re.search(r"(_compress|_thumbnail)", f) ])
                .apply(" | ".join))                                                   # revert list back to string
        return self;

    def downloader(self):
        """download all the images"""
        files = self.df["fichier(s)"].str.split(r"\s*\|\s*", regex=True).explode()
        print(f"* downloading {len(files)} images from the server")

        for f in tqdm.tqdm(files, desc="* downloading files"):
            r = requests.get(f"{self.url}{f}", stream=True)
            if r.status_code != 200:
                raise requests.exceptions.RequestException(
                    f"HTTP status code: `{r.status_code}`. could not fetch image `{f}` from URL `{self.url}`. if you're downloading from a local server, ensure it is running.")
            with open(os.path.join(self.pathtmp, "images", f), mode="wb") as fh:
                for chunk in r:
                    fh.write(chunk)
            pass
        # write metadata files
        self.df.to_csv(os.path.join(self.pathtmp, "metadonnees_export_images.csv"),  # type:ignore
                       sep="\t",
                       header=True,
                       index=False)
        self.df.to_excel(os.path.join(self.pathtmp, "metadonnees_export_images.xlsx"),  # type:ignore
                         header=True,
                         index=False)

        return self

    def zipper(self):
        """zip all the data to the output"""
        print("* zipping files to output")
        shutil.make_archive(str(self.pathout), "zip", self.pathtmp)
        print(f"* archive written to : {self.pathout}.zip")
        return self

# *****************************************

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="imgdl",
                                     description="providing a file containing a list of iconography.id_uuids, generate an export of iconography rows and images to a zipfile",
                                     usage="python imgdl.py -f <filename>")
    parser.add_argument("-u", "--urlcontext",
                        choices=["local", "remote"],
                        required=True,
                        help="fetch from a local or remote app. this will be used to define the URLs. if it's a local app, make sure it's running")
    parser.add_argument("-f", "--filename",
                        required=True,
                        help=r"path to a file containing the iconography rows to export, with 1 iconography.id_uuid / line, each iconography.id_uuid matching the regex qr1[a-z0-9]{32}")
    parser.add_argument("-q", "--fullquality",
                        default=False,
                        help="only retrieve full quality images (not the compressed or thumbnail version)",
                        action="store_true")
    args = parser.parse_args()

    ImageDownloader(args.filename, args.urlcontext, args.fullquality).pipeline()
